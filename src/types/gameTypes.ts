export type Cell = {
    x: number,
    y: number
}

type RowRotation = {
    type: 'row',
    direction: 'left' | 'right'
}

type ColumnRotation = {
    type: 'column',
    direction: 'up' | 'down'
}

export type Rotation = {
    position: number,

    rotationType: RowRotation | ColumnRotation

}

type InterrogateMove = {
    type: 'interrogate',
    cell: Cell,
}

type KillMove = {
    type: 'kill',
    cell: Cell
}

type RotationMove = {
    type: 'rotation',
    rotation: Rotation
}

export type Move = InterrogateMove | KillMove | RotationMove