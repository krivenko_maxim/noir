import * as process from "process";

export const fieldSize: number = 6

export const serverAddress: string = process.env.SERVER || 'localhost:5000'