import React, {useState, useEffect} from "react";
interface LocalStorageProps<T> {
    key: string,
    initial: T
}
export function useLocalStorage<T>({key, initial} : LocalStorageProps<T>) {
    const [value, setValue] = useState<T>(() => {
        const value = localStorage.getItem(key)
        return value ? JSON.parse(value) : initial
    })

    useEffect(() => {
        const item = JSON.stringify(value)
        localStorage.setItem(key, item)
    }, [value])

    return [value, setValue]
}