import React, {useState, useEffect, useRef, useReducer} from 'react'
import socket_io, {Socket} from 'socket.io-client'
import {Move} from '../types/gameTypes'
import {validateMove} from "../utils/validateMove";
import {serverAddress} from "../const/constants";
import _ from 'lodash'

interface MoveProps {
    gameId: string
}

const updateField = (prevState, action) => {

}

export function useMove({gameId}: MoveProps) {
    const [isConnected, setIsConnected] = useState(false)
    const [isYourMove, setIsYourMove] = useState(false)
    const [gameField, dispatchMove] = useReducer(updateField, )

    const socketRef = useRef<Socket | null>(null)

    useEffect(() => {
        socketRef.current = socket_io(serverAddress, {
            query: {gameId}
        })

        socketRef.current?.emit('player:join', {

        })

        socketRef.current?.on('connected', () => {
            setIsConnected(true)
        })

        socketRef.current?.on('your move', () => {
            setIsYourMove(true)
        })

        return () => {
            socketRef.current?.disconnect()
            setIsConnected(false)
        }
    }, [])

    const makeMove = (moveType: Move) => {
        if (!validateMove(moveType)) {
            return //TODO make error message once move is invalid
        }
        setIsYourMove(false)
        socketRef.current?.emit('move', moveType)
    }

    return {
        isConnected,
        isYourMove,
        makeMove,
        gameField
    }
}
