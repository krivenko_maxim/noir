import React from 'react';
import PropTypes from 'prop-types';
import styles from './layout.module.scss'
import {Outlet} from "react-router-dom";
function Layout() {
    return (
        <div className={styles.container}>
            <div/>
            <div className={styles['inner-container']}>
                <Outlet/>
            </div>
            <div/>
        </div>
    );
}

export default Layout;