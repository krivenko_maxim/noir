import React from 'react';
import styles from './gamepage.module.scss'
import {useBeforeUnload, useParams} from "react-router-dom";
import {useMove} from "../../hooks/useMove";

interface GamePageProps {

}

function GamePage({}: GamePageProps) {
    const { gameId } = useParams()

    const {} = useMove({gameId})

    return (
        <div className={styles.container}>
            {gameId}
        </div>
    );
}


export default GamePage;