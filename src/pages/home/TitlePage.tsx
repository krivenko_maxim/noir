import React from 'react';
import styles from './titlepage.module.scss'
import Button from "../../components/button/Button";
interface TitlePageProps {

}
function TitlePage({} : TitlePageProps) {
    return (
        <div className={styles.container}>
            <Button>
                Начать игру
            </Button>
            <Button>
                Правила
            </Button>
        </div>
    );
}

export default TitlePage;