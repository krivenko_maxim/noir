import {Cell, Move, Rotation} from '../types/gameTypes'
import {fieldSize} from "../const/constants";
function validateCell({x, y}: Cell): boolean {
    return x >= 0 && x < fieldSize && y >= 0 && y < fieldSize
}

function validateRotation({position}: Rotation): boolean {
    return position >= 0 && position < fieldSize
}

export function validateMove(moveType: Move) {
    switch (moveType.type) {
        case 'interrogate':
        case "kill":
            return validateCell(moveType.cell)
        case 'rotation':
            return validateRotation(moveType.rotation)
    }
}