import {createBrowserRouter} from "react-router-dom";
import TitlePage from "../pages/home/TitlePage";
import GamePage from "../pages/game/GamePage";
import Layout from "../pages/layout/Layout";

const router = createBrowserRouter([
    {
        path: '/',
        element: <Layout />,
        children: [
            {
                path: '',
                element: <TitlePage />,
            },
            {
                path: '/game/:gameId',
                element: <GamePage />
            }
        ]
    },
])

export default router