import React, {MouseEventHandler, ReactNode} from 'react';
import classNames from "classnames";
import styles from './button.module.scss'

interface ButtonProps {
    onClick?: MouseEventHandler<HTMLButtonElement>,
    children?: ReactNode | ReactNode[]
    style?: 'default' | 'tertiary'
}

function Button({onClick, children, style = 'default'}: ButtonProps) {
    return (
        <button
            className={classNames(styles.btn, styles[style])}
            onClick={onClick}
        >
            {children}
        </button>
    )
}

export default Button;